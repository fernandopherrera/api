//version inicial

var express = require('express'),
  app = express(),//nos simplifica la vida XP
  port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

var movimientosJSONv2 = require('./movimientosv2.json')

console.log('todo list RESTful API server started on: ' + port);
app.get('/',function(req,res) {
  //res.send("Hola mundo desde NodeJS");
  res.sendFile(path.join(__dirname,'index.html'));
})

app.post('/', function(req, res) {
  res.send("Hemos recibido su petición POST");//http://localhost:3000/v2/Movimientos

})

app.put('/', function(req, res) {
  res.send("Hemos recibido su petición PUT");

})

app.delete('/', function(req, res) {
  res.send("Hemos recibido su petición DELETE");

})

app.get('/Clientes', function (req, res) {
  res.send("Aquí tiene a los clientes");

})

app.get('/Clientes/:idcliente', function (req, res) {
  res.send("Aquí tiene al cliente: " + req.params.idcliente);

})

app.get('/v1/Movimientos', function(req, res){
  res.sendfile('movimientosv1.json');
})

app.get('/v2/Movimientos', function(req, res){
  res.json(movimientosJSONv2);
})

app.get('/v2/Movimientos/:id', function(req, res){
  console.log(req.params.id);
  res.json(movimientosJSONv2[req.params.id - 1]);
})


//Query
app.get('/v2/Movimientosq', function(req, res){
  console.log(req.query);
  res.send("Petición con quiery recibida y cambiada: " + req.query);
})

app.get('/v2/Movimientos/:id/:nombre', function(req, res){
  console.log(req.params);
  console.log(req.params.id);
  console.log(req.params.nombre);
  res.send("recibido");
})
